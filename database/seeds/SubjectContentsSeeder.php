<?php

use Illuminate\Database\Seeder;
use App\Subject;
use App\SubjectContent;
use App\Tutorial;
use App\Quiz;

class SubjectContentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects = Subject::all();
        $tutorials = Tutorial::all();
        $quizzes = Quiz::all();
        foreach ($subjects as $subject) {

          foreach ($tutorials as $tutorial) {
            SubjectContent::create(
              ['subject_id' => $subject->id,
              'contentable_id' => $tutorial->id,
              'contentable_type' => 'Tutorial']);
          }

          foreach ($quizzes as $quiz) {
            SubjectContent::create(
              ['subject_id' => $subject->id,
              'contentable_id' => $quiz->id,
              'contentable_type' => 'Quiz']);
          }

        }
    }
}
