<?php

use Illuminate\Database\Seeder;
use \App\Course;


class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $primary = Course::create(['title' => 'Primary']);
        $secondary = Course::create(['title' => 'Secondary']);


        $primary->subjects()->createMany([
            ['title' => 'English'],
            ['title' => 'Mathematics'],
            ['title' => 'Science'] ]);

        $secondary->subjects()->createMany([
            ['title' => 'French'],
            ['title' => 'Social Studies'],
            ['title' => 'Swahili'] ]);

    }
}
