<?php

use Illuminate\Database\Seeder;
use App\Quiz;
use App\Question;
use App\QuestionQuiz;

class QuizzesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $quiz1 = Quiz::create(['title' => 'What is the largest continent in the world?']);
      $quiz2 = Quiz::create(['title' => 'Latitudes move which direction on a map?']);
      $quiz3 = Quiz::create(['title' => 'What is the largest planet in our galaxy?']);
      $quiz4 = Quiz::create(['title' => 'What is the fastest land animal?']);
      $quiz5 = Quiz::create(['title' => 'The higher you go, the cooler it becomes?']);



      Question::insert([
        ['content' => 'America'],
        ['content' => 'Asia'],
        ['content' => 'Europe'],
        ['content' => 'Africa'],
        ['content' => 'East to West'],
        ['content' => 'North to South'],
        ['content' => 'Earth'],
        ['content' => 'Satan'],
        ['content' => 'Jupiter'],
        ['content' => 'Jaguar'],
        ['content' => 'Leopard'],
        ['content' => 'Cheetah'],
        ['content' => 'True'],
        ['content' => 'False']]);


      $quiz1_questions = Question::offset(0)->limit(4)->get();
      $quiz2_questions = Question::offset(4)->limit(2)->get();
      $quiz3_questions = Question::offset(6)->limit(3)->get();
      $quiz4_questions = Question::offset(9)->limit(3)->get();
      $quiz5_questions = Question::offset(12)->limit(2)->get();

      foreach ($quiz1_questions as $question) {
        QuestionQuiz::create(['question_id' => $question->id, 'quiz_id' => $quiz1->id ]);
      }

      foreach ($quiz2_questions as $question) {
        QuestionQuiz::create(['question_id' => $question->id, 'quiz_id' => $quiz2->id ]);
      }

      foreach ($quiz3_questions as $question) {
        QuestionQuiz::create(['question_id' => $question->id, 'quiz_id' => $quiz3->id ]);
      }

      foreach ($quiz4_questions as $question) {
        QuestionQuiz::create(['question_id' => $question->id, 'quiz_id' => $quiz4->id ]);
      }

      foreach ($quiz5_questions as $question) {
        QuestionQuiz::create(['question_id' => $question->id, 'quiz_id' => $quiz5->id ]);
      }

    }
}
