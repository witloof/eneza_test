<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call(UsersTableSeeder::class);
      $this->call(CoursesTableSeeder::class);
      $this->call(TutorialsTableSeeder::class);
      $this->call(QuizzesTableSeeder::class);
      $this->call(SubjectContentsSeeder::class);
    }
}
