<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/* Courses */
Route::get('courses', 'CourseController@index');
Route::get('courses/{course}', 'CourseController@show');
Route::post('courses', 'CourseController@store');
Route::put('courses/{course}', 'CourseController@update');

/* Subjects */
Route::get('subjects', 'SubjectController@index');
Route::get('subjects/{subject}', 'SubjectController@show');
Route::post('subjects', 'SubjectController@store');
Route::put('subjects/{subject}', 'SubjectController@update');

/* Tutorials */
Route::get('tutorials', 'TutorialController@index');
Route::get('tutorials/{tutorial}', 'TutorialController@show');
Route::post('tutorials', 'TutorialController@store');
Route::put('tutorials/{tutorial}', 'TutorialController@update');

/* Quizzes */
Route::get('quizzes', 'QuizController@index');
Route::get('quizzes/{quiz}', 'QuizController@show');
Route::post('quizzes', 'QuizController@store');
Route::put('quizzes/{quiz}', 'QuizController@update');

/* Questions */
Route::get('questions', 'QuestionController@index');
Route::get('questions/{question}', 'QuestionController@show');
Route::post('questions', 'QuestionController@store');
Route::put('questions/{question}', 'QuestionController@update');
