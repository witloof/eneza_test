<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Subject;

class QuestionQuiz extends Model
{
  protected $fillable = ['question_id', 'quiz_id'];

  public function question()
  {
    return $this->belongsTo('App\Question');
  }

  public function quiz()
  {
    return $this->belongsTo('App\Quiz');
  }

}
