<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Subject;
class Course extends Model
{
  protected $fillable = ['title'];

  public function subjects()
  {
      return $this->hasMany('App\Subject');
  }
}
