<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
  protected $fillable = ['title', 'course_id'];

  public function contents()
  {
      return $this->hasMany('App\SubjectContent');
  }

  public function course()
  {
    return $this->belongsTo('App\Course');
  }
}
