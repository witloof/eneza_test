<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class SubjectContent extends Model
{
    protected $fillable = [
        'subject_id', 'content_id', 'content_type',
    ];

}
