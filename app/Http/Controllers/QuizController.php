<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use App\Http\Resources\Quiz as QuizResource;

class QuizController extends Controller
{
    public function index()
    {
        return QuizResource::collection(Quiz::all());
    }

    public function create()
    {
        return $quiz;
    }


    public function show(Quiz $quiz)
    {
        return new QuizResource($quiz);
    }

    public function store(Request $request)
    {

        $quiz = Quiz::create($request->all());

        return response()->json($quiz, 201);
    }

    public function update(Request $request, Quiz $quiz)
    {
        $quiz->update($request->all());

        return response()->json($quiz, 200);
    }

    public function delete(Quiz $quiz)
    {
        $quiz->delete();

        return response()->json(null, 204);
    }
}
