<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\Http\Resources\Subject as SubjectResource;

class SubjectController extends Controller
{
    public function index()
    {
        return SubjectResource::collection(Subject::all());
    }

    public function create()
    {
        return $subject;
    }


    public function show(Subject $subject)
    {
        return new SubjectResource($subject);
    }

    public function store(Request $request)
    {

        $subject = subject::create($request->all());

        return response()->json($subject, 201);
    }

    public function update(Request $request, Subject $subject)
    {
        $subject->update($request->all());

        return response()->json($subject, 200);
    }

    public function delete(Subject $subject)
    {
        $subject->delete();

        return response()->json(null, 204);
    }
}
