<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tutorial;
use App\Http\Resources\Tutorial as TutorialResource;

class TutorialController extends Controller
{
    public function index()
    {
        return TutorialResource::collection(Tutorial::all());
    }

    public function create()
    {
        return $tutorial;
    }


    public function show(Tutorial $tutorial)
    {
        return new TutorialResource($tutorial);
    }

    public function store(Request $request)
    {

        $tutorial = Tutorial::create($request->all());

        return response()->json($tutorial, 201);
    }

    public function update(Request $request, Tutorial $tutorial)
    {
        $tutorial->update($request->all());

        return response()->json($tutorial, 200);
    }

    public function delete(Tutorial $tutorial)
    {
        $tutorial->delete();

        return response()->json(null, 204);
    }
}
