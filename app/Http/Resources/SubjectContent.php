<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubjectContent extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'contentable_id' => $this->contentable_id,
            'contentable_type' => $this->contentable_type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
